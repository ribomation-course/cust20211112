#include <iostream>
#include <fstream>
#include <sstream>
#include <filesystem>
#include <stdexcept>
#include <algorithm>
#include <numeric>
#include <string_view>

#include <cstring>
#include <cerrno>

#include <unistd.h>
#include <sys/wait.h>

#include "shared-memory.hxx"

using namespace std;
using namespace ribomation::shm;
namespace fs = std::filesystem;


auto load(const string& filename, size_t filesize, SharedMemory& shm) -> string_view;
auto count(string_view s, const string& phrase) -> unsigned;
void childProcess(unsigned id, unsigned* sum, string_view segment, const string& phrase);

int main(int argc, char** argv) {
    auto phrase     = "file"s;
    auto fileName   = "../app.cxx"s;
    auto numWorkers = 2U;

    for (auto k = 1; k < argc; ++k) {
        string arg = argv[k];
        if (arg == "-p"s) {
            phrase = argv[++k];
        } else if (arg == "-f"s) {
            fileName = argv[++k];
        } else if (arg == "-w"s) {
            numWorkers = stoi(argv[++k]);
        } else {
            cerr << "usage: " << argv[0] << " [-p <phrase>] [-f <file>] [-w <workers>]\n";
            return 1;
        }
    }

    auto fileSize     = fs::file_size(fileName);
    auto partialsSize = numWorkers * sizeof(unsigned);
    auto shmSize      = fileSize + partialsSize;
    auto shm          = SharedMemory{shmSize};

    auto segmentSize  = fileSize / numWorkers;
    auto segmentSum   = reinterpret_cast<unsigned*>(reinterpret_cast<unsigned long>(shm.data()) + fileSize);
    fill_n(segmentSum, numWorkers, 0);

    auto content = load(fileName, fileSize, shm);

    for (auto k = 0U; k < numWorkers; ++k) {
        auto pid = fork();
        if (pid == -1) throw runtime_error{"fork(2) failed: "s + strerror(errno)};
        if (pid > 0) continue;

        auto segment = content.substr(k * segmentSize, segmentSize);
        childProcess(k + 1, &segmentSum[k], segment, phrase);
    }

    for (auto k = 0U; k < numWorkers; ++k) {
        wait(nullptr);
    }

    auto totalSum = accumulate(segmentSum, segmentSum + numWorkers, 0);
    cout << "phrase '" << phrase << "' occurs " << totalSum << "\n";
}

void childProcess(unsigned id, unsigned* sum, string_view segment, const string& phrase) {
    {
        auto buf = ostringstream{};
        buf << "worker-" << id << " started\n";
        cout << buf.str();
    }

    *sum = count(segment, phrase);

    {
        auto buf = ostringstream{};
        buf << "worker-" << id << " done\n";
        cout << buf.str();
    }

    exit(0);
}

auto load(const string& filename, size_t filesize, SharedMemory& shm) -> string_view {
    auto file = ifstream{filename};
    if (!file) throw invalid_argument{"cannot open"s + filename};

    file.read(static_cast<char*>(shm.data()), filesize);

    return {static_cast<char*>(shm.data()), filesize};
}

unsigned count(string_view s, const string& phrase) {
    auto const N   = phrase.size();
    auto       sum = 0U;

    for (auto it = s.find(phrase); it != string_view::npos; it = s.find(phrase, it + N)) ++sum;

    return sum;
}
