#pragma once

#include <string>
#include <sstream>
#include <stdexcept>
#include <iterator>

#include <cstring>
#include <cerrno>
#include <cmath>

#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>


namespace ribomation::shm {
    using namespace std;
    using namespace std::literals;

    struct SharedMemory {
        SharedMemory(size_t shmSize, void* startAddr = 0, const string& shmName = "/cxx-shm"s) {
            name  = (shmName.at(0) == '/') ? shmName : "/"s + shmName;
            size_  = alignSize(shmSize);
            begin_ = mkShm(name, startAddr, size_);
            end_     = reinterpret_cast<char*>(begin_) + size_;
            nextAddr = begin_;
        }

        ~SharedMemory() {
            munmap(begin_, size_);
            shm_unlink(name.c_str());
        }

        void* data() const { return begin_; }
        size_t size() const { return size_; }
        void* begin() const { return begin_; }
        void* end() const { return end_; }

        void* allocate(size_t numBytes) {
            void* addr = nextAddr;
            nextAddr = reinterpret_cast<char*>(nextAddr) + numBytes;
            if (end_ <= nextAddr) throw overflow_error{"SHM"s};
            return addr;
        }

        template<typename T>
        T* allocate(unsigned numElems = 1) {
            return reinterpret_cast<T*>(allocate(numElems * sizeof(T)));
        }

        SharedMemory() = delete;
        SharedMemory(const SharedMemory&) = delete;
        SharedMemory& operator =(const SharedMemory&) = delete;

    private:
        size_t size_;
        string name;
        void* begin_;
        void* end_;
        void* nextAddr;

       static void* mkShm(const string& name, void* startAddr, size_t size) {
            auto shmFd = shm_open(name.c_str(), O_CREAT | O_TRUNC | O_RDWR, 0600);
            if (shmFd == -1) fail("shm_open");

            if (ftruncate(shmFd, size) == -1) fail("ftruncate");

            auto shm = mmap(startAddr, size, PROT_READ | PROT_WRITE, MAP_SHARED, shmFd, 0);
            if (shm == MAP_FAILED) fail("mmap");

            close(shmFd);
            return shm;
        }

        static auto alignSize(size_t size) -> size_t {
            auto pageSize = static_cast<double>(getpagesize());
            return static_cast<size_t>(ceil(size / pageSize) * pageSize);
        }

        static void fail(const string& funcName) {
            auto buf = ostringstream{};
            buf << funcName << "(): " << strerror(errno);
            throw runtime_error{buf.str()};
        }
    };

}
