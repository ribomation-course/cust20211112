#pragma once
#include <stdexcept>

namespace ribomation {
    using byte = unsigned char;
    using std::bad_alloc;

    template<unsigned CAPACITY = 1024>
    class MonotonicAllocator {
        byte storage[CAPACITY]{};
        byte* lastAddr = storage;

    public:
        byte* allocate(unsigned numBytes) {
            auto addr = lastAddr;
            lastAddr += numBytes;
            if (lastAddr > (storage + CAPACITY)) {
                throw bad_alloc{};
            }
            return addr;
        }
    };
}
