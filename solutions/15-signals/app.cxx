
#include <iostream>
#include <sstream>
#include <functional>
#include <stdexcept>
#include <chrono>
#include <csignal>
#include <cerrno>
#include <cstring>
#include <unistd.h>

using namespace std;
using namespace std::literals;
using namespace std::chrono;

auto exceptionStartTime = steady_clock::now();

void handleTimeout(int signo) {
    exceptionStartTime = steady_clock::now();
    auto buf           = ostringstream{};
    buf << strsignal(signo) << " (" << signo << ")";
    throw overflow_error(buf.str());
}

void setupSignal() {
    sigset_t mask;
    sigemptyset(&mask);
    sigaddset(&mask, SIGALRM);
    if (sigprocmask(SIG_UNBLOCK, &mask, nullptr) != 0)
        throw invalid_argument("sigprocmask: "s + strerror(errno));

    struct sigaction action{};
    action.sa_handler = &handleTimeout;
    if (sigaction(SIGALRM, &action, nullptr) != 0)
        throw invalid_argument("sigaction: "s + strerror(errno));
}

template<typename ArgType, typename ReturnType>
ReturnType invokeWithTimeout(function<ReturnType(ArgType)> f, ArgType n, unsigned maxSeconds) {
    setupSignal();
    alarm(maxSeconds);
    try {
        auto result = f(n);
        alarm(0);
        return result;
    } catch (overflow_error& x) {
        auto elapsed = steady_clock::now() - exceptionStartTime;
        cout << "\nGot timeout: " << x.what()
             << "\nElapsed time of exception handling: " << duration_cast<microseconds>(elapsed).count() << " us"
             << endl;
    }
    return {};
}

// --------------------------------------
unsigned long fib(unsigned n) {
    if (n == 0) return 0;
    if (n == 1) return 1;
    return fib(n - 2) + fib(n - 1);
}

int main(int argc, char** argv) {
    unsigned arg     = (argc >= 2) ? stoi(argv[1]) : 42U;
    unsigned timeout = (argc >= 3) ? stoi(argv[2]) : 4U;

    cout << "fib(" << arg << ") ... " << flush;

    auto start  = steady_clock::now();
    auto result = invokeWithTimeout<unsigned, unsigned long>(fib, arg, timeout);
    auto end    = steady_clock::now();

    cout << "\nfib(" << arg << ") = " << result
         << "\nElapsed time: " << duration_cast<milliseconds>(end - start).count() << " ms\n";

    return 0;
}

