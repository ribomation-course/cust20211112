cmake_minimum_required(VERSION 3.16)
project(03_system_heap)

set(CMAKE_CXX_STANDARD 17)
set(WARN -Wall -Wextra -Werror -Wfatal-errors)


add_executable(out-of-memory out-of-memory.cxx)
target_compile_options(out-of-memory PRIVATE ${WARN})

add_executable(mmap-threshold mmap-threshold.cxx)
target_compile_options(mmap-threshold PRIVATE ${WARN})

add_executable(memory-leak memory-leak.cxx)
target_compile_options(memory-leak PRIVATE ${WARN})
