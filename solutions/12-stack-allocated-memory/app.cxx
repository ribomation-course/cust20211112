#include <iostream>
#include <string>
#include <alloca.h>

using namespace std;

struct Person {
    string const   name = ""s;
    unsigned const age = 0;
    Person* const next = nullptr;

    Person(string name, unsigned age, Person* next)
            : name{std::move(name)}, age{age}, next{next} {}

    Person() = default;

};

void print(Person* node) {
    if (node == nullptr) return;
    cout << "Person{" << node->name << ", " << node->age << "} @ " << node << "\n";
    print(node->next);
}

void dispose(Person* node) {
    if (node == nullptr) return;
    node->~Person();
    dispose(node->next);
}


void run(unsigned numPersons) {
   // Person arr[numPersons];
    Person* list = nullptr;
    auto      prefix = "nissenissenisse-"s;
    for (auto k      = numPersons; k > 0; --k) {
        auto mem = alloca(sizeof(Person));
        list = new(mem) Person{prefix + to_string(k), 20U + k, list};
    }
    print(list);
    dispose(list);
}

int main() {
    auto const N = 10U;
    run(N);
}
