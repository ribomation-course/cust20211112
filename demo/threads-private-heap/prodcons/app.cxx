#include <cstdio>
#include <cstring>
#include <string>
#include <string_view>
#include <memory_resource>
#include <thread>
#include <future>

#include "message-queue.hxx"
#include "message.hxx"

using std::data;
using std::size;
using std::thread;
using std::promise;
using std::future;
using std::stoi;
using std::string_view;
using namespace std::string_view_literals;

using std::pmr::monotonic_buffer_resource;
using std::pmr::unsynchronized_pool_resource;
using std::pmr::null_memory_resource;

using ribomation::threads::MessageQueue;
using ribomation::threads::Message;


struct Receiver {
    using MESSAGE = Message<42>;

    void start() {
        thread kicker{&Receiver::body, this};
        runner.swap(kicker);
        fullyStarted.get_future().wait();
    }

    void join() { runner.join(); }

    void send(unsigned id, string_view data) {
        auto addr = heap->allocate(sizeof(MESSAGE));
        auto msg  = new (addr) MESSAGE{id, data};
        inbox.put(msg);
    }

private:
    thread                          runner;
    MessageQueue<MESSAGE*, 2>       inbox;
    unsynchronized_pool_resource*   heap;
    promise<bool>                   fullyStarted;

    void body() {
        char storage[10'000];
        auto buffer = monotonic_buffer_resource{data(storage), size(storage),
                                                null_memory_resource()};
        auto pool   = unsynchronized_pool_resource{&buffer};
        heap = &pool;
        fullyStarted.set_value(true);
        run();
    }

    void run() {
        bool running = true;
        do {
            auto msg = inbox.get();

            auto id = msg->getId();
            if (id == 0) { running = false; }

            char payload[MESSAGE::MAX_SIZE];
            msg->copyTo(payload, sizeof(payload));

            msg->~MESSAGE();
            heap->deallocate(msg, sizeof(MESSAGE));

            printf("[receiver] (%d) '%s'\n", id, payload);
        } while (running);
    }
};

struct Sender {
    Sender(unsigned numMessages, Receiver* receiver)
            : numMessages(numMessages), receiver(receiver) {}

    void start() {
        thread kicker{&Sender::run, this};
        runner.swap(kicker);
    }

    void join() { runner.join(); }

private:
    unsigned    numMessages;
    Receiver*   receiver;
    thread      runner;

    void run() {
        for (auto k = 1U; k <= numMessages; ++k) {
            receiver->send(k, "Hi from the sender thread"sv);
        }
        receiver->send(0U, ""sv);
    }
};


int main(int argc, char** args) {
    auto const N = (argc == 1) ? 10U : stoi(args[1]);

    auto receiver = Receiver{};
    receiver.start();

    auto sender = Sender{N, &receiver};
    sender.start();

    sender.join();
    receiver.join();

    return 0;
}


