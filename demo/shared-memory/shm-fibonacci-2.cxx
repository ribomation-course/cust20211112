#include <iostream>
#include "shared-memory.hxx"
#include <chrono>
#include "data.hxx"

using namespace std;
using namespace std::literals;
using namespace std::chrono;
using namespace ribomation::shm;
using namespace ribomation::data;

void generator(Data* arr, int N) {
    cout << "[child] generating " << N << " results...\n";
    auto      startTime = high_resolution_clock::now();
    for (auto k         = 0; k < N; ++k) {
        arr[k].arg    = k + 1;
        arr[k].result = fibonacci(arr[k].arg);
    }
    auto      endTime   = high_resolution_clock::now();
    auto      elapsed   = duration_cast<milliseconds>(endTime - startTime).count();
    cout << "[child] elapsed " << elapsed / 1000.0 << " seconds\n";
}

void printer(Data* arr, int N) {
    cout << "[parent] printing " << N << " results\n";
    for (auto k = 0; k < N; ++k) { cout << arr[k] << "\n"; }
    for (auto k = 0; k < N; ++k) { arr[k].~Data(); }
}

int main() {
    auto numValues = 10;
    auto shm       = SharedMemory{numValues * sizeof(Data)};
    auto data      = new(shm.allocate<Data>(numValues)) Data[numValues];
    auto rc        = fork();
    if (rc == -1)
        throw invalid_argument{"cannot create child process: "s + strerror(errno)};
    if (rc == 0) {
        generator(reinterpret_cast<Data*>(data), numValues);
        exit(0);
    }
    wait(nullptr);
    printer(reinterpret_cast<Data*>(data), numValues);
    shm.deallocate(data);
    return 0;
}


