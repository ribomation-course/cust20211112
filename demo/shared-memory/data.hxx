#pragma once

#include <iosfwd>

namespace ribomation::data {
    using namespace std;

    struct Data {
        int           arg;
        unsigned long result;
        Data();
        ~Data();
    };

    auto fibonacci(int n) -> unsigned long;
    auto operator <<(ostream&, const Data&) -> ostream&;

}

