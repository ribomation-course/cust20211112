#pragma once

#include <functional>
#include <pthread.h>
#include "mutex.hxx"

class Condition {
    Mutex& mutex;
    pthread_cond_t cond;

public:
    Condition(Mutex& m) : mutex{m} {
        pthread_condattr_t cfg;
        pthread_condattr_init(&cfg);

        pthread_condattr_setpshared(&cfg, PTHREAD_PROCESS_SHARED);
        pthread_cond_init(&cond, &cfg);

        pthread_condattr_destroy(&cfg);
    }

    ~Condition() {
        pthread_cond_destroy(&cond);
    }

    void waitUntil(std::function<bool()> until) {
        while (!until()) wait();
    }

    void wait() {
        pthread_cond_wait(&cond, mutex.native());
    }

    void notify() {
        pthread_cond_signal(&cond);
    }

    void notifyAll() {
        pthread_cond_broadcast(&cond);
    }

    Condition() = delete;
    Condition(const Condition&) = delete;
    Condition& operator=(const Condition&) = delete;
};
