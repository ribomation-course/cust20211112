#include <iostream>
#include "data.hxx"
#include "static-block-pool.hxx"
#include "no-heap.hxx"

using namespace std;
using namespace ribomation::data;
using namespace ribomation::memory;

auto func(SmartPtr<Data> ptr) {
    ptr->ival *= 10;
    ptr->dval *= ptr->dval;
    return ptr;
}

int main() {
    auto pool = StaticBlockPool<Data, 5>{};
    {
        auto obj = make_smart_ptr<Data>(pool, 42, 3.14);
        obj->print("obj=");

        auto q = func(std::move(obj));
        printf("(1) pool.in-use=%d\n", pool.allocated());
        q->print("q=");
        if (obj) { obj->print("obj="); } else { printf("obj=<null>\n"); }

        auto p = make_smart_ptr<Data>(pool, 10, 1.);
        printf("(2) pool.in-use=%d\n", pool.allocated());
        p->print("p=");

        q = std::move(p);
        printf("(3) pool.in-use=%d\n", pool.allocated());
        q->print("q=");
        if (p) { p->print("p="); } else { printf("p=<null>\n"); }
        if (obj) { obj->print("obj="); } else { printf("obj=<null>\n"); }
    }
    printf("(outside) pool.in-use=%d\n", pool.allocated());
    noHeapCheck();
}
