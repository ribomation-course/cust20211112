#pragma once

namespace ribomation::ptr {

    template<typename T>
    struct SimplePtr {
        SimplePtr() = default;
        SimplePtr(T* addr) : addr{addr} {};
        ~SimplePtr() { delete addr; }

        auto operator ->() -> T* { return addr; }
        auto operator  *() -> T& { return *addr; }
        operator  bool() { return addr != nullptr; }

        SimplePtr(const SimplePtr<T>&)                        = delete;
        auto operator =(const SimplePtr<T>&) -> SimplePtr<T>& = delete;
        auto operator &() const -> const void*                = delete;
    private:
        T* addr = nullptr;
    };

}


