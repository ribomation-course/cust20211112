#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <memory>
#include <random>
#include "person.hxx"
#include "trace.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::persons;
using namespace ribomation::util;

using Repo  = map<string, shared_ptr<Person>>;
using Cache = map<string, weak_ptr<Person>>;

auto load() -> Repo {
    auto t    = Trace{"load"};
    auto repo = Repo{};
    repo["a"s] = make_shared<Person>("Anna", 27);
    repo["b"s] = make_shared<Person>("Bertil", 27);
    repo["c"s] = make_shared<Person>("Carin", 27);
    repo["d"s] = make_shared<Person>("Dan", 27);
    repo["e"s] = make_shared<Person>("Eva", 27);
    repo["f"s] = make_shared<Person>("Filip", 27);
    return repo;
}

auto populate_cache(const Repo& repo) -> Cache {
    auto t     = Trace{"populate cache"};
    auto cache = Cache{};
    for (auto const&[key, val] : repo) cache[key] = val;
    return cache;
}

void drop_some(Cache& cache) {
    auto t    = Trace{"drop some"};
    auto keys = vector<string>{};
    keys.reserve(cache.size());
    for (auto const&[key, val] : cache) keys.push_back(key);

    auto r = random_device{};
    auto d = uniform_int_distribution<unsigned long>{0, cache.size() - 1};
    auto N = d(r);
    t.out() << "N=" << N << endl;
    while (N-- > 0) {
        auto key = keys[d(r)];
        cache[key].reset();
        t.out() << "dropped " << key << endl;
    }
}

void print(const Repo& repo) {
    auto t = Trace{"print repo"};
    for (auto const&[key, val] : repo)
        t.out() << key << ": " << val->toString() << endl;
}

void print(Cache& cache) {
    auto t = Trace{"print cache"};
    for (auto const&[key, val] : cache)
        t.out() << key << ": "
                << (val.expired() ? "<expired>"s : val.lock()->toString())
                << endl;
}

int main() {
    auto t = Trace{"main"};
    {
        auto repo = load();
        print(repo);
        auto cache = populate_cache(repo);
        print(cache);
        drop_some(cache);
        print(cache);
        print(repo);
    }
    return 0;
}
