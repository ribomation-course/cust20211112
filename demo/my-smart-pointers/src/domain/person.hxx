#pragma once

#include <string>
#include <sstream>
#include <utility>
#include "trace.hxx"

namespace ribomation::persons {
    using namespace std;
    using namespace std::literals;
    using namespace ribomation::util;

    class Person {
        const string name;
        int          age;
        Trace        t;
    public:
        Person(const string& name, int age) : name{name}, age{age}, t{name + ", "s + to_string(age), this} {}

        auto toString() const {
            auto buf = ostringstream{};
            buf << "Person{" << name << ", " << age << "} @ " << this;
            return buf.str();
        }

        auto getName() const { return name; }

        auto getAge() const { return age; }

        int incrAge() { return ++age; }
    };

}
