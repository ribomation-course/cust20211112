#include <iostream>

#include "simple-ptr.hxx"
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation::ptr;
using namespace ribomation::persons;


int main() {
    {
        auto ptr = SimplePtr<Person>{new Person{"Per Silja", 37}};
        if (ptr) (*ptr).incrAge();
        cout << "(1) ptr: " << ptr->toString() << endl;
    }
    {
        auto ptr = SimplePtr<Person>{};
        cout << "(2) ptr: " << (ptr ? ptr->toString() : "<null>") << endl;
    }
    return 0;
}

