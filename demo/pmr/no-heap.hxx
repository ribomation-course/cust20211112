#pragma once
#include <cstddef>
#include <new>

auto operator new(size_t numBytes) -> void* {
    throw std::bad_alloc{};
}
auto operator new[](size_t numBytes) -> void* {
    throw std::bad_alloc{};
}

