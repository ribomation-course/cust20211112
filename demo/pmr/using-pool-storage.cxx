#include <vector>
#include <string>
#include <memory_resource>
#include <cstdio>
#include "no-heap.hxx"

using std::data;
using std::size;
using std::move;

using std::pmr::monotonic_buffer_resource;
using std::pmr::unsynchronized_pool_resource;
using std::pmr::null_memory_resource;
using std::pmr::set_default_resource;

using std::pmr::vector;
using std::pmr::string;

struct spy_monotonic_buffer_resource : monotonic_buffer_resource {
    using super = monotonic_buffer_resource;

    spy_monotonic_buffer_resource(void* buffer, size_t bufferSize, memory_resource* upstream)
            : super(buffer, bufferSize, upstream) {
        printf("[buf] created, size=%ld\n", bufferSize);
    }

    void printStats(const char* msg) {
        printf("[buf] [%s] current: %ld bytes, max: %ld bytes\n", msg, curAllocBytes, maxAllocBytes);
    }

protected:
    void* do_allocate(size_t bytes, size_t alignment) override {
        printf("[buf] allocate(%ld bytes, align=%ld)\n", bytes, alignment);
        auto blk = super::do_allocate(bytes, alignment);
        curAllocBytes += bytes;
        maxAllocBytes += bytes;
        printf("[buf]  -> %p (cur=%ld, max=%ld)\n", blk, curAllocBytes, maxAllocBytes);
        return blk;
    }

    void do_deallocate(void* p, size_t bytes, size_t alignment) override {
        printf("[buf] deallocate(%p, %ld bytes, %ld)\n", p, bytes, alignment);
        super::do_deallocate(p, bytes, alignment);
        curAllocBytes -= bytes;
        printf("[buf]  -> (cur=%ld, max=%ld)\n", curAllocBytes, maxAllocBytes);
    }

    bool do_is_equal(const memory_resource& __other) const noexcept override {
        return super::do_is_equal(__other);
    }

private:
    long maxAllocBytes = 0;
    long curAllocBytes = 0;
};


struct spy_unsynchronized_pool_resource : unsynchronized_pool_resource {
    using super = unsynchronized_pool_resource;

    spy_unsynchronized_pool_resource(memory_resource* upstream) : super(upstream) {
        printf("[pool] created: blocks_per_chunk=%ld, largest_block=%ld\n",
               options().max_blocks_per_chunk, options().largest_required_pool_block);
    }

    void printStats(const char* msg) {
        printf("[pool] [%s] current: %ld bytes, max: %ld bytes\n", msg, curAllocBytes, maxAllocBytes);
    }

protected:
    void* do_allocate(size_t bytes, size_t alignment) override {
        printf("[pool] allocate(%ld bytes, align=%ld)\n", bytes, alignment);
        auto blk = super::do_allocate(bytes, alignment);
        curAllocBytes += bytes;
        maxAllocBytes += bytes;
        printf("[pool]  -> %p (cur=%ld, max=%ld)\n", blk, curAllocBytes, maxAllocBytes);
        return blk;
    }

    void do_deallocate(void* p, size_t bytes, size_t alignment) override {
        printf("[pool] deallocate(%p, %ld bytes, %ld)\n", p, bytes, alignment);
        super::do_deallocate(p, bytes, alignment);
        curAllocBytes -= bytes;
        printf("[pool]  -> (cur=%ld, max=%ld)\n", curAllocBytes, maxAllocBytes);
    }

    bool do_is_equal(const memory_resource& other) const noexcept override {
        return super::do_is_equal(other);
    }

private:
    long maxAllocBytes = 0;
    long curAllocBytes = 0;
};


void run() {
    char storage[30'000U];
    auto upstream = null_memory_resource();
    auto buffer   = spy_monotonic_buffer_resource{data(storage), size(storage), upstream};
    auto pool     = spy_unsynchronized_pool_resource{&buffer};
    set_default_resource(&pool);

    buffer.printStats("before block");
    pool.printStats("before block");
    {
        auto words = vector<string>{};
        buffer.printStats("vector created");
        pool.printStats("vector created");

        for (auto k = 1UL; k <= 9; ++k) {
            auto txt = string(k * 10, '.');
            txt[0] = '0' + k;
            printf("** '%s'\n", txt.c_str());
            words.push_back(move(txt));
            buffer.printStats("after push");
            pool.printStats("after push");
        }

        for (auto const& w : words) printf("[main] w: '%s'\n", w.c_str());
        buffer.printStats("block end");
        pool.printStats("block end");
    }
    buffer.printStats("after block");
    pool.printStats("after block");
}

int main() {
    run();
    try {
        auto p = new int{42};
        printf("unexpected: %p !!!\n", p);
    } catch (std::bad_alloc& x) {
        printf("Confirmed: No heap memory was used!\n");
    }
    return 0;
}

