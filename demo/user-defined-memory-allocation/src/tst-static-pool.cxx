#include <array>
#include <cstdio> //printf
#include "no-heap.hxx"
#include "data.hxx"
#include "static-block-pool.hxx"
#include "smart-pointer-support.hxx"

using namespace ribomation::memory;
using namespace ribomation::data;

int main(int argc, char** argv) {
    constexpr auto N    = 5U;
    auto           vec  = std::array<Data*, N>{};
    auto           pool = StaticBlockPool<Data, N>{};

    printf("N            = %d\n", N);
    printf("sizeof(Data) = %ld\n", sizeof(Data));
    printf("sizeof(pool) = %ld\n", sizeof(pool));
    printf("&pool        = %p\n", &pool);

    for (auto k = 0; !pool.full(); ++k) {
        vec[k] = new (pool.allocate()) Data{k + 1, 3.1415 * (k + 1)};
    }
    for (auto ptr : vec) {
        printf("* Data(%d, %f)\n", ptr->ival, ptr->dval);
    }
    for (auto ptr : vec) {
        ptr->~Data();
        pool.deallocate(ptr);
    }
    if (pool.available() != N) {
        throw std::invalid_argument{"allocation mismatch"};
    }
    try {
        auto p = new int{42};
        printf("unexpected: %p !!!\n", p);
    } catch (std::bad_alloc& x) {
        printf("No heap memory used!\n");
    }
    return 0;
}


