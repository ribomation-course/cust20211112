#pragma once
#include <numeric>
#include <string_view>

namespace ribomation::app {
    using namespace std;

    struct WordFreq {
        string_view   word{};
        unsigned      count{};
        unsigned long sum{};

        WordFreq() = default;

        explicit WordFreq(string_view w) : word{w}, count{1} {
            sum = accumulate(word.begin(), word.end(), 0);
        }
    };

    inline bool operator==(const WordFreq& lhs, const WordFreq& rhs) {
        return (lhs.sum == rhs.sum) && (lhs.word == rhs.word);
    }

    inline bool operator<(const WordFreq& lhs, const WordFreq& rhs) {
        return lhs.count > rhs.count;
    }

}

