#include <iostream>
#include <fstream>
#include <string>
#include <string_view>
#include <tuple>
#include <optional>
#include <algorithm>
#include <numeric>
#include <chrono>
#include <filesystem>
#include "lib/memory-segment.hxx"
#include "lib/scan-word.hxx"
#include "word-freq.hxx"

using namespace std;
using namespace std::chrono;
using namespace ribomation::memory;
using namespace ribomation::app;
namespace fs = std::filesystem;

auto parseArgs(int argc, char** argv) -> tuple<string, unsigned, unsigned>;
auto load(const string& fileName, unsigned long fileSize, MemorySegment& memory) -> string_view;
auto aggregate(string_view& content, MemorySegment& memory, unsigned minWordSize) -> tuple<WordFreq*, unsigned>;

int main(int argc, char** argv) {
    cout.imbue(locale{"en_US.UTF8"});
    auto[fileName, maxNumWords, minWordSize] = parseArgs(argc, argv);

    auto startTime = high_resolution_clock::now();
    auto fileSize   = fs::file_size(fileName);
    auto memorySize = 3 * fileSize;
    auto memory     = MemorySegment{memorySize};

    auto content    = load(fileName, fileSize, memory);
    cout << "Loaded " << fileName << ": " << content.size() << " chars\n";

    auto [wfArr, numWords] = aggregate(content, memory, minWordSize);
    cout << "Parsed " << numWords << " words\n";

    partial_sort(&wfArr[0], &wfArr[maxNumWords], &wfArr[numWords]);
    for_each_n(&wfArr[0], maxNumWords, [](const WordFreq& wf) {
        cout << wf.word << ": " << wf.count << "\n";
    });

    auto endTime     = high_resolution_clock::now();
    auto elapsedTime = duration_cast<seconds>(endTime - startTime).count();
    cout << "Elapsed " << elapsedTime << " seconds\n";
}


auto parseArgs(int argc, char** argv) -> tuple<string, unsigned, unsigned> {
    auto fileName    = "./files/shakespeare.txt"s;
    auto maxNumWords = 10U;
    auto minWordSize = 4U;

    for (int k = 1; k < argc; ++k) {
        string arg = argv[k];
        if (arg == "--file"s) {
            fileName = argv[++k];
        } else if (arg == "--max"s) {
            maxNumWords = stoi(argv[++k]);
        } else if (arg == "--min"s) {
            minWordSize = stoi(argv[++k]);
        } else {
            cerr << "usage: " << argv[0] << " [--file <filename>] [---max <number>] [---min <number>]\n";
            exit(1);
        }
    }

    if (!fs::exists(fileName) || !fs::is_regular_file(fileName)) {
        cerr << argv[0] << ": not a file: " << fileName << "\n";
        exit(2);
    }

    return {fileName, maxNumWords, minWordSize};
}

auto load(const string& fileName, unsigned long fileSize, MemorySegment& memory) -> string_view {
    auto file = ifstream{fileName};
    if (!file) {
        throw runtime_error{"cannot open/read "s + fileName};
    }
    auto fileBegin = memory.allocate(fileSize);
    file.read(fileBegin, static_cast<streamsize>(fileSize));
    return {fileBegin, fileSize};
}

auto aggregate(string_view& content, MemorySegment& memory, unsigned minWordSize) -> tuple<WordFreq*, unsigned> {
    WordFreq* wfArr = nullptr;
    auto numWords = 0U;

    for (auto word = nextWord(content); word; word = nextWord(content)) {
        if (word->size() < static_cast<unsigned long>(minWordSize)) {
            continue;
        }

        if (wfArr == nullptr) {
            wfArr    = new(memory.allocate(sizeof(WordFreq))) WordFreq{*word};
            numWords = 1;
        } else {
            auto maybe = WordFreq{*word};
            auto it    = find(&wfArr[0], &wfArr[numWords], maybe);
            if (it == &wfArr[numWords]) { //not found
                new(memory.allocate(sizeof(WordFreq))) WordFreq{*word};
                ++numWords;
            } else {
                it->count++;
            }
        }
    }

    return {wfArr, numWords};
}
