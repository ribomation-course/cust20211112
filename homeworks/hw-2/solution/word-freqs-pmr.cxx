#include <iostream>
#include <string_view>
#include <string>
#include <algorithm>
#include <tuple>
#include <chrono>
#include <unordered_map>
#include <vector>
#include <random>
#include <memory_resource>

#include <cstdio>
#include <cstring>

#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "lib/memory-segment.hxx"
#include "lib/scan-word.hxx"
#include "lib/spy-pmr.hxx"
#include "lib/file-mgr.hxx"
#include "lib/no-heap.hxx"

using namespace std::chrono;
using namespace ribomation::memory;
using namespace ribomation::app;
using namespace ribomation::io;

using std::pmr::memory_resource;
using std::pmr::monotonic_buffer_resource;
using std::pmr::unsynchronized_pool_resource;
using std::pmr::null_memory_resource;
using std::string_view;

using WordFreqs = std::pmr::unordered_map<string_view, unsigned>;
using Pair = std::pair<string_view, unsigned>;
using Sortable = std::pmr::vector<Pair>;

auto file_size(const char* filename) -> size_t;
auto parseArgs(int argc, char** argv) -> std::tuple<char*, unsigned, unsigned>;
auto load(const char* fileName, unsigned long fileSize, MemorySegment& memory) -> string_view;
auto aggregate(string_view& text, MemorySegment& mem, unsigned minWord) -> WordFreqs;
void store(Sortable& sortable, unsigned N, const char* filename);


int main(int argc, char** argv) {
    auto startTime = high_resolution_clock::now();

    auto[fileName, maxNumWords, minWordSize] = parseArgs(argc, argv);
    auto fileSize  = file_size(fileName);
    if (fileSize == 0) {
        fprintf(stderr, "cannot open '%s'\n", fileName);
        exit(1);
    }

    auto memorySize = 2 * fileSize;
    auto memory     = MemorySegment{memorySize};

    auto storageSize = memorySize - fileSize;
    auto storage     = memory.allocate(storageSize);
    auto upstream    = null_memory_resource();
    auto buffer      = monotonic_buffer_resource{storage, storageSize, upstream};
    auto spy         = spy_pmr{buffer, storageSize};
    auto pool        = unsynchronized_pool_resource{&spy};
    std::pmr::set_default_resource(&pool);

    {
        auto content = load(fileName, fileSize, memory);
        printf("Loaded %s: %ld chars\n", fileName, content.size());

        auto freqs = aggregate(content, memory, minWordSize);
        printf("Parsed %d words\n", static_cast<unsigned>(freqs.size()));

        auto sortable = Sortable{freqs.begin(), freqs.end()};
        std::partial_sort(sortable.begin(), sortable.begin() + maxNumWords, sortable.end(), [](auto lhs, auto rhs) {
            return lhs.second > rhs.second;
        });

        std::for_each_n(sortable.begin(), maxNumWords, [](const Pair& wf) {
            auto& word = wf.first;
            auto count = wf.second;
            printf("%.*s: %d\n",
                   static_cast<int>(word.size()), word.data(), count);
        });

        store(sortable, maxNumWords, "./word-freqs.html");
    }

    auto endTime     = high_resolution_clock::now();
    auto elapsedTime = duration_cast<milliseconds>(endTime - startTime).count();
    printf("Elapsed %ld ms\n", elapsedTime);

    spy.stats();
    ensure_no_heap();
}

auto file_size(const char* filename) -> size_t {
    struct stat data{};

    auto rc = stat(filename, &data);
    if (rc == 0) return static_cast<size_t>(data.st_size);

    return 0UL;
}

auto parseArgs(int argc, char** argv) -> std::tuple<char*, unsigned, unsigned> {
    static char fileName_default[] = "./files/shakespeare.txt";
    char* fileName = fileName_default;
    auto maxNumWords = 100U;
    auto minWordSize = 5U;

    auto EQ = [](const char* lhs, const char* rhs) {
        return ::strcmp(lhs, rhs) == 0;
    };

    for (int k = 1; k < argc; ++k) {
        char* arg = argv[k];
        if (EQ(arg, "--file")) {
            fileName = argv[++k];
        } else if (EQ(arg, "--max")) {
            maxNumWords = atoi(argv[++k]);
        } else if (EQ(arg, "--min")) {
            minWordSize = atoi(argv[++k]);
        } else {
            fprintf(stderr, "usage: %s [--file <filename>] [---max <number>] [---min <number>]\\n", argv[0]);
            exit(1);
        }
    }

    return {fileName, maxNumWords, minWordSize};
}

auto load(const char* fileName, unsigned long fileSize, MemorySegment& memory) -> string_view {
    auto fd = open(fileName, O_RDONLY);
    if (fd < 0) {
        fprintf(stderr, "open(2) failed [%s]: %s (%d)\n", fileName, strerror(errno), errno);
        exit(1);
    }

    auto fileBegin = memory.allocate(fileSize);
    auto rc        = read(fd, fileBegin, fileSize);
    if (rc < 0) {
        fprintf(stderr, "read(2) failed: %s (%d)\n", strerror(errno), errno);
        exit(2);
    }

    return {fileBegin, fileSize};
}

auto aggregate(string_view& text, MemorySegment& mem, unsigned minWord) -> WordFreqs {
    auto freqs = WordFreqs{};

    for (auto word = nextWord(text); word; word = nextWord(text)) {
        if (word->size() < static_cast<unsigned long>(minWord)) {
            continue;
        }
        ++freqs[*word];
    }

    return freqs;
}

void store(Sortable& sortable, unsigned N, const char* filename) {
    using std::pmr::string;
    auto maxFreq   = sortable[0].second;
    auto minFreq   = sortable[N - 1].second;
    auto maxFont   = 150.0;
    auto minFont   = 15.0;
    auto scale     = (maxFont - minFont) / (maxFreq - minFreq);
    auto devRandom = std::random_device{};

    auto tags = std::pmr::vector<string>{};
    tags.reserve(N);

    std::for_each_n(sortable.begin(), N, [&tags, scale, &devRandom](const Pair& wf) {
        auto& word = wf.first;
        auto count = wf.second;
        auto size  = static_cast<int>(scale * count);
        char buf[512];

        auto randColor = [&devRandom]() -> string {
            char buf[32];
            auto nextByte = std::uniform_int_distribution<unsigned char>{0, 255};
            sprintf(buf, "#%02X%02X%02X",
                    nextByte(devRandom), nextByte(devRandom), nextByte(devRandom));
            return buf;
        };

        sprintf(buf, "<span style=\"font-size: %dpx; color: %s; margin-right: 1rem;\">%.*s</span>",
                size,
                randColor().c_str(),
                static_cast<int>(word.size()), word.data());

        tags.emplace_back(buf);
    });
    std::shuffle(tags.begin(), tags.end(), devRandom);

    auto prefix = R"(
        <html>
        <style>
            body { font-family: sans-serif; }
        </style>
        <body>
        <h1>Word Frequencies</h1>
    )";

    auto html = FileMgr{filename};
    fprintf(html, "%s\n", prefix);
    std::for_each(tags.begin(), tags.end(), [&html](const string& tag) {
        fprintf(html, "%s\n", tag.c_str());
    });
    fprintf(html, "</body></html>\n");
}

