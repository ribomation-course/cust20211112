#pragma once
#include <cstddef>

namespace ribomation::memory {

    class MemorySegment {
        void* storageBegin;
        size_t storageSize;
        void* storageEnd;
        void* nextAddress;

    public:
        explicit MemorySegment(size_t size);
        ~MemorySegment();
        char* allocate(unsigned numBytes);

        MemorySegment() = delete;
        MemorySegment(const MemorySegment&) = delete;
        MemorySegment(MemorySegment&&) = delete;
        MemorySegment& operator=(const MemorySegment&) = delete;
        MemorySegment& operator=(MemorySegment&&) = delete;
    };


}
