#pragma once

#include <vector>

namespace ribomation::memory {

    class MemorySegment {
        void* memSegmentStart = nullptr;
        size_t            memSegmentSize = 0;
        std::vector<bool> inUse{};
        unsigned          nextIndex      = 0;
        const size_t      objectSize;

    public:
        explicit MemorySegment(size_t objectSize_) : objectSize{objectSize_} {
        }

        [[maybe_unused]] MemorySegment(unsigned capacity, size_t objectSize_) : objectSize{objectSize_} {
            setup(capacity);
        }

        ~MemorySegment() {
            shutdown();
        }

        MemorySegment() = delete;
        MemorySegment(const MemorySegment&) = delete;
        MemorySegment(MemorySegment&&) = delete;
        MemorySegment& operator=(MemorySegment&&) = delete;

        void setup(unsigned capacity);
        void shutdown();

        void* allocate();
        void deallocate(void* ptr);
    };

}
