#include <iostream>
#include <algorithm>
#include <random>
#include "account.hxx"

namespace ribomation::demo {
    using namespace std;
    using namespace ribomation::memory;

    MemorySegment Account::pool{sizeof(Account)};
    bool          Account::verbose{true};
    random_device r{};

    Account::Account() : balance{0} {
        setAccno(""s);
        if (verbose) cout << "Account{} @ " << this << "\n";
    }

    Account::Account(const string& accno_, int balance_) : balance{balance_} {
        setAccno(accno_);
        if (verbose) cout << "Account{" << getAccno() << ", " << getBalance() << "} @ " << this << "\n";
    }

    Account::~Account() {
        if (verbose) cout << "~Account{} @ " << this << "\n";
    }

    string Account::getAccno() const {
        auto result = string{accno, ACCNO_SIZE};
        auto pos    = result.find_first_of(PAD_CHAR);
        return (pos == string::npos) ? result : result.substr(0, pos);
    }

    void Account::setAccno(const string& s) {
        fill(accno, accno + ACCNO_SIZE, PAD_CHAR);
        s.copy(accno, ACCNO_SIZE);
    }

    string nextAccno() {
        auto letter = uniform_int_distribution<char>{'A', 'Z'};
        auto digit  = uniform_int_distribution<char>{'0', '9'};

        auto accno = ""s;
        generate_n(back_inserter(accno), 3, [&letter]() {
            return letter(r);
        });
        accno.push_back('-');
        generate_n(back_inserter(accno), 6, [&digit]() {
            return digit(r);
        });

        return accno;
    }

    int nextBalance() {
        auto number = normal_distribution<float>{100, 75};
        return static_cast<int>(number(r));
    }

}
